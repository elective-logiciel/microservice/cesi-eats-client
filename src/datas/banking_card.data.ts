import { BankingCard } from "../models/banking_card.model";
import { mssql, sqlConfig } from "../services/mssql.service"
const sql = require('mssql')

const mssqlDB = new mssql();

export class BankingCardData {
    public async GetBankingCardByUserId(id_user: number) {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, id_user)

        const res = await request.query('SELECT Id_Bankings, Name, Card_Number, Security_Code, Expiration_Date FROM Banking_Cards WHERE Id_Users = @id_user')

        pool.close()

        return res
    }

    public async InsertBankingCard(banking_card: BankingCard) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('name', sql.NVarChar, banking_card.name)
        request.input('card_number', sql.NVarChar, banking_card.card_number)
        request.input('security_code', sql.NVarChar, banking_card.security_code)
        request.input('expiration_date', sql.NVarChar, banking_card.expiration_date)
        request.input('id_user', sql.Int, banking_card.id_user)

        await request.query('INSERT INTO Banking_Cards (Name, Card_Number, Security_Code, Expiration_Date, Id_Users) VALUES (@name, @card_number, @security_code, @expiration_date, @id_user)')

        pool.close()
    }

    public async DeleteBankingCardById(id_banking_card: number) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_banking_card', sql.Int, id_banking_card)

        await request.query('DELETE FROM Banking_Cards WHERE Id_Bankings = @id_banking_card')

        pool.close()
    }

    public async DeleteAllBankingCarsByIdUser(id_user: number) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, id_user)

        await request.query('DELETE FROM Banking_Cards WHERE Id_Users = @id_user')

        pool.close()
    }
}