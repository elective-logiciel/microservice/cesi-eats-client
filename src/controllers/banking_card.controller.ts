import { Router } from 'express';
import { BankingCardData } from '../datas/banking_card.data';
import { BankingCard, convertQueryResToBankingCardList } from '../models/banking_card.model';
import { Error404 } from '../errors/errors';
import { returnCreated, returnDeleted, returnSuccess } from '../errors/success';

const bankingCardController = Router();
const MssqlDB = new BankingCardData();

// Get all banking cards from a client with a client id
bankingCardController.get('/:id_user(\\d+)', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const query_res = await MssqlDB.GetBankingCardByUserId(req.params.id_user)
        if (query_res.rowsAffected[0] === 0) {
            throw new Error404('Banking cards not found, this user does not have any banking card.')
        }

        const banking_card: BankingCard[] = convertQueryResToBankingCardList(query_res)

        var message = "Get banking cards by id_user"

        returnSuccess(res, banking_card, message)

    } catch (err) {
        next(err)
    }
})

// Add banking card with user id
bankingCardController.post('/add', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const banking_card: BankingCard = new BankingCard({
            name: req.body.name,
            card_number: req.body.card_number,
            security_code: req.body.security_code,
            expiration_date: req.body.expiration_date,
            id_user: req.body.id_user,
        })

        banking_card.IsNameValid()
        banking_card.IsCardNumberValid()
        banking_card.IsSecurityCodeValid()
        banking_card.IsExpirationDateValid()
        banking_card.IsIdUserValid()

        await MssqlDB.InsertBankingCard(banking_card)

        var message = "Add banking card by id_user"

        returnCreated(res, message)
        
    } catch (err) {
        next(err)
    }
})

// Delete banking card by banking card id
bankingCardController.delete('/delete/:id_banking_card(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        await MssqlDB.DeleteBankingCardById(req.params.id_banking_card)

        var message = "Delete banking card by id_banking_card"

        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

// Delete all address by user id 
bankingCardController.delete('/delete/all/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        await MssqlDB.DeleteAllBankingCarsByIdUser(req.params.id_user)

        var message = "Delete all banking cards by id_user"

        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

export { bankingCardController };