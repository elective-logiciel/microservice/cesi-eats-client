import { Error400 } from "../errors/errors";

export class BankingCard {
    id_banking_card?: number
    name?: string
    card_number?: string
    security_code?: string
    expiration_date?: string
    id_user?: number

    public constructor(init?: Partial<BankingCard>) {
        Object.assign(this, init);
    }

    public IsIdBankingCardValid(): boolean {
        if (this.id_banking_card === undefined) {
            throw new Error400("Missing argument, 'id_banking_card' can not be NULL")
        }
        if (this.id_banking_card < 0) {
            throw new Error400("Out of range argument, 'id_banking_card' can not be a negative number")
        }
        return true
    }

    public IsNameValid(): boolean {
        if (this.name === undefined) {
            throw new Error400("Missing argument, 'name' can not be NULL")
        }
        if (this.name === "") {
            throw new Error400("Empty argument, 'name' can not be EMPTY")
        }
        return true
    }

    public IsCardNumberValid(): boolean {
        if (this.card_number === undefined) {
            throw new Error400("Missing argument, 'card_number' can not be NULL")
        }
        if (this.card_number === "") {
            throw new Error400("Empty argument, 'card_number' can not be EMPTY")
        }
        return true
    }

    public IsSecurityCodeValid(): boolean {
        if (this.security_code === undefined) {
            throw new Error400("Missing argument, 'security_code' can not be NULL")
        }
        if (this.security_code === "") {
            throw new Error400("Empty argument, 'security_code' can not be EMPTY")
        }
        return true
    }

    public IsExpirationDateValid(): boolean {
        if (this.expiration_date === undefined) {
            throw new Error400("Missing argument, 'expiration_date' can not be NULL")
        }
        if (this.expiration_date === "") {
            throw new Error400("Empty argument, 'expiration_date' can not be EMPTY")
        }
        return true
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }
}

// Convert the result send by the database into a array of banking card struct
export function convertQueryResToBankingCardList(query_res: any): BankingCard[] {
    let banking_card_list: BankingCard[] = []

    for (let i = 0; i < query_res.rowsAffected[0]; i++) {
        let res_banking_card: any = query_res.recordset[i]

        banking_card_list.push(new BankingCard({
            id_banking_card: res_banking_card.Id_Bankings,
            name: res_banking_card.Name,
            card_number: res_banking_card.Card_Number,
            security_code: res_banking_card.Security_Code,
            expiration_date: res_banking_card.Expiration_Date,
            id_user: res_banking_card.Id_User,
        }))
    }

    return banking_card_list
}
