import { Error400 } from "../../src/errors/errors"
import { Address } from "../../src/models/address.model"

const valid_address: Address = new Address({
    id_address: 1,
    street: 'street',
    zip_code: 'zip_code',
    city: 'city',
    id_user: 1,
})

const unvalid_address: Address = new Address({
    id_address: -1,
    street: '',
    zip_code: '',
    city: '',
    id_user: -1,
})

const undefined_address: Address = new Address({})

describe('Address structure field are correct', () => {
    test('Id address is set and valid', () => {
        const address: Address = valid_address

        expect(address.IsIdAddressValid()).toBeTruthy()
    })

    test('Street is set and valid', () => {
        const address: Address = valid_address

        expect((address.IsStreetValid())).toBeTruthy()
    })


    test('Zip code is set and valid', () => {
        const address: Address = valid_address

        expect(address.IsZipCodeValid()).toBeTruthy()
    })


    test('City is set and valid', () => {
        const address: Address = valid_address

        expect(address.IsCityValid()).toBeTruthy()
    })


    test('Id user is set and valid', () => {
        const address: Address = valid_address

        expect(address.IsIdUserValid()).toBeTruthy
    })
})

describe('Address structure field are undefined', () => {
    test('Id address is set and unvalid', () => {
        const address: Address = unvalid_address
        const err: Error400 = new Error400("Out of range argument, 'id_address' can not be a negative number")
        
        function caughtError() {
            address.IsIdAddressValid()
        }

        expect(caughtError).toThrowError(err)
    })

    test('Street is set and unvalid', () => {
        const address: Address = unvalid_address
        const err: Error400 = new Error400("Empty argument, 'street' can not be EMPTY")

        function caughtError() {
            address.IsStreetValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Zip code is set and unvalid', () => {
        const address: Address = unvalid_address
        const err: Error400 = new Error400("Empty argument, 'zip_code' can not be EMPTY")

        function caughtError() {
            address.IsZipCodeValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('City is set and unvalid', () => {
        const address: Address = unvalid_address
        const err: Error400 = new Error400("Empty argument, 'city' can not be EMPTY")

        function caughtError() {
            address.IsCityValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Id user is set and unvalid', () => {
        const address: Address = unvalid_address
        const err: Error400 = new Error400("Out of range argument, 'id_user' can not be a negative number")

        function caughtError() {
            address.IsIdUserValid()
        }

        expect(caughtError).toThrow(err)
    })
})

describe('Address structure field are undefined', () => {
    test('Id address is not set and undefined', () => {
        const address: Address = undefined_address
        const err: Error400 = new Error400("Missing argument, 'id_address' can not be NULL")

        function caughtError() {
            address.IsIdAddressValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Street is not set and undefined', () => {
        const address: Address = undefined_address
        const err: Error400 = new Error400("Missing argument, 'street' can not be NULL")

        function caughtError() {
            address.IsStreetValid()
        }

        expect(caughtError).toThrow(err)
    })


    test('Zip code is not set and undefined', () => {
        const address: Address = undefined_address
        const err: Error400 = new Error400("Missing argument, 'zip_code' can not be NULL")

        function caughtError() {
            address.IsZipCodeValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('City is not set and undefined', () => {
        const address: Address = undefined_address
        const err: Error400 = new Error400("Missing argument, 'city' can not be NULL")

        function caughtError() {
            address.IsCityValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Id user is not set and undefined', () => {
        const address: Address = undefined_address
        const err: Error400 = new Error400("Missing argument, 'id_user' can not be NULL")

        function caughtError() {
            address.IsIdUserValid()
        }

        expect(caughtError).toThrow(err)
    })
})
