import { Error400 } from "../../src/errors/errors"
import { BankingCard } from "../../src/models/banking_card.model"

const valid_banking_card: BankingCard = new BankingCard({
    id_banking_card: 1,
    name: 'name',
    card_number: '011011113333',
    security_code: '123',
    expiration_date: '01/01',
    id_user: 1,
})

const unvalid_banking_card: BankingCard = new BankingCard({
    id_banking_card: -1,
    name: '',
    card_number: '',
    security_code: '',
    expiration_date: '',
    id_user: -1,
})

const undefined_banking_card: BankingCard = new BankingCard({})

describe('Banking card structure field are correct', () => {
    test('Id banking card is set and valid', () => {
        const banking_card: BankingCard = valid_banking_card

        expect(banking_card.IsIdBankingCardValid()).toBeTruthy()
    })

    test('Name is set and valid', () => {
        const banking_card: BankingCard = valid_banking_card

        expect((banking_card.IsNameValid())).toBeTruthy()
    })

    test('Card number is set and valid', () => {
        const banking_card: BankingCard = valid_banking_card

        expect((banking_card.IsCardNumberValid())).toBeTruthy()
    })


    test('Security code is set and valid', () => {
        const banking_card: BankingCard = valid_banking_card

        expect(banking_card.IsSecurityCodeValid()).toBeTruthy()
    })


    test('Expiration date is set and valid', () => {
        const banking_card: BankingCard = valid_banking_card

        expect(banking_card.IsExpirationDateValid()).toBeTruthy()
    })


    test('Id user is set and valid', () => {
        const banking_card: BankingCard = valid_banking_card

        expect(banking_card.IsIdUserValid()).toBeTruthy
    })
})

describe('Banking card structure field are undefined', () => {
    test('Id banking card is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Out of range argument, 'id_banking_card' can not be a negative number")

        function caughtError() {
            banking_card.IsIdBankingCardValid()
        }

        expect(caughtError).toThrowError(err)
    })

    test('Name is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Empty argument, 'name' can not be EMPTY")

        function caughtError() {
            banking_card.IsNameValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Card number is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Empty argument, 'card_number' can not be EMPTY")

        function caughtError() {
            banking_card.IsCardNumberValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Security code is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Empty argument, 'security_code' can not be EMPTY")

        function caughtError() {
            banking_card.IsSecurityCodeValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Expiration date is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Empty argument, 'expiration_date' can not be EMPTY")

        function caughtError() {
            banking_card.IsExpirationDateValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Id user is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Out of range argument, 'id_user' can not be a negative number")

        function caughtError() {
            banking_card.IsIdUserValid()
        }

        expect(caughtError).toThrow(err)
    })
})

describe('Banking card structure field are undefined', () => {
    test('Id banking card is not set and undefined', () => {
        const banking_card: BankingCard = undefined_banking_card
        const err: Error400 = new Error400("Missing argument, 'id_banking_card' can not be NULL")

        function caughtError() {
            banking_card.IsIdBankingCardValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Name is not set and undefined', () => {
        const banking_card: BankingCard = undefined_banking_card
        const err: Error400 = new Error400("Missing argument, 'name' can not be NULL")

        function caughtError() {
            banking_card.IsNameValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Card number is not set and undefined', () => {
        const banking_card: BankingCard = undefined_banking_card
        const err: Error400 = new Error400("Missing argument, 'card_number' can not be NULL")

        function caughtError() {
            banking_card.IsCardNumberValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Security code is set and unvalid', () => {
        const banking_card: BankingCard = unvalid_banking_card
        const err: Error400 = new Error400("Empty argument, 'security_code' can not be EMPTY")

        function caughtError() {
            banking_card.IsSecurityCodeValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Expiration date is not set and undefined', () => {
        const banking_card: BankingCard = undefined_banking_card
        const err: Error400 = new Error400("Missing argument, 'expiration_date' can not be NULL")

        function caughtError() {
            banking_card.IsExpirationDateValid()
        }

        expect(caughtError).toThrow(err)
    })

    test('Id user is not set and undefined', () => {
        const banking_card: BankingCard = undefined_banking_card
        const err: Error400 = new Error400("Missing argument, 'id_user' can not be NULL")

        function caughtError() {
            banking_card.IsIdUserValid()
        }

        expect(caughtError).toThrow(err)
    })
})
